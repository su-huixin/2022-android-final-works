package com.example.myapp.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.view.GestureDetectorCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapp.Json.Data;
import com.example.myapp.R;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHoder> implements View.OnClickListener{

    //声明一个上下文的对象（后续的getView()方法当中，会用到LayoutInflater加载XML布局）
    private Context context;
    private List<Data> dataList;
    private RecyclerView recyclerView;

    private OnItemClickListener onItemClickListener;
    private View.OnLongClickListener longClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener){
        this.onItemClickListener = onItemClickListener;
    }



    public interface OnItemClickListener{
        //参数（父组件，当前单击的View,单击的View的位置，数据）
        void onItemClick(RecyclerView parent, View view, int position, Data data);
    }



    //构造方法
    public NewsAdapter(Context context, List<Data> dataList,RecyclerView recyclerView){
        this.context = context;
        this.dataList = dataList;
        this.recyclerView=recyclerView;
    }

    //决定第position处的列表项组件
    @NonNull
    @Override
    public NewsHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(context).inflate(R.layout.news_item, null);
            NewsHoder newsHoder=new NewsHoder(view);
            view.setOnClickListener(this);
            return newsHoder;

    }



    @Override
    public void onBindViewHolder(@NonNull NewsHoder holder, int position) {
        Data data = dataList.get(position);
        holder.tvTitle.setText(data.getTitle());
        holder.tvAuth.setText(data.getAuthor_name());
        holder.tvTime.setText(data.getDate());
        String pic_url = data.getThumbnail_pic_s();
        setPicBitmap(holder.ivPic, pic_url);


    }




    //决定第position处的列表项ID
    @Override
    public long getItemId(int position) {
        return position;
    }

    //控制该Adapter包含列表项的个数
    @Override
    public int getItemCount() {
       return dataList.size();
    }




    public static void setPicBitmap(final ImageView ivPic, final String pic_url){

        //设置图片需要访问网络，因此不能在主线程中设置
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    HttpURLConnection conn = (HttpURLConnection)new URL(pic_url).openConnection();
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    Bitmap bitmap = BitmapFactory.decodeStream(is);
                    ivPic.setImageBitmap(bitmap);
                    is.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        int position = recyclerView.getChildAdapterPosition(v);
        //程序执行到此，会去执行具体实现的onItemClick()方法
        if (onItemClickListener!=null){
            onItemClickListener.onItemClick(recyclerView,v,position,dataList.get(position));
        }
    }

    class NewsHoder extends RecyclerView.ViewHolder{

        TextView tvTitle, tvAuth ,tvTime;
        ImageView ivPic;

        public NewsHoder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvAuth =itemView.findViewById(R.id.tvAuth);
            tvTime =itemView.findViewById(R.id.tvTime);
            ivPic = itemView.findViewById(R.id.ivPic);

        }
    }
}