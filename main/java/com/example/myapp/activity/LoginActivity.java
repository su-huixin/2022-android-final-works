package com.example.myapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.myapp.R;
import com.example.myapp.Utils.Declare;
import com.example.myapp.Utils.UserUtils;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private Button login;
    private EditText name,pwd;
    private String un,upwd;
    private CheckBox checkBox,loginck;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private TextView forget,register;

    @Override
    protected int initLayout() { return R.layout.activity_login;
    }

    @Override
    protected void initView() {
        login=findViewById(R.id.login_btn_login);
        name=findViewById(R.id.login_et_name);
        pwd=findViewById(R.id.login_et_pwd);
        loginck=findViewById(R.id.login_ck);
        forget=findViewById(R.id.login_forget);
        register=findViewById(R.id.login_register);
        checkBox =  findViewById(R.id.login_checkBox);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isRemember= sharedPreferences.getBoolean("remember_password", false);
       save(isRemember);
    }



    @Override
    protected void initData() {
        login.setOnClickListener(this);
        forget.setOnClickListener(this);
        register.setOnClickListener(this);
    }





    @Override
    public void onClick(View v) {
        un=name.getText().toString();
        upwd=pwd.getText().toString();
        Intent intent=new Intent();

        switch (v.getId()){
            case R.id.login_btn_login:
                login(un,upwd);
                break;
            case R.id.login_register:
                intent=new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                break;
            case R.id.login_forget:
                intent=new Intent(LoginActivity.this,ForgetActivity.class);
                startActivity(intent);
                break;
        }


    }
    public void  Plaintext(View view){
        //更改密码显示方式的方法
        boolean a=false;
        a=loginck.isChecked();
        xsmm(a,pwd);
    }

    private void login(String account, String pwd) {
        editor= sharedPreferences.edit();
        UserUtils userService = new UserUtils(this);
        boolean flag = userService.login(account, pwd);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (account.equals("") || pwd.equals("")) {
                    showToast("账号或密码为空");
                } else {
                    if (flag == true) {
                        showToast("登陆成功");
                            Declare.getInstance().setAccounts(account);
                            System.out.println(Declare.getInstance().getAccounts());
                        Intent intent=new Intent(LoginActivity.this,HomeActivity.class);
                        startActivity(intent);
                        if (checkBox.isChecked()) {
                            editor.putString("Name", account);
                            editor.putString("Password", pwd);
                            editor.putBoolean("remember_password", true);
                        } else {
                            editor.clear();
                        }
                        editor.apply();

                    } else {
                        showToast("登陆失败");
                    }
                }
            }
        });
    }

    public void save(boolean isRemember){
        if (isRemember) {
            name.setText(sharedPreferences.getString("Name", ""));
             pwd.setText(sharedPreferences.getString("Password", ""));
            checkBox.setChecked(true);
        }
    }
}
