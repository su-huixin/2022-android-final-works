package com.example.myapp.activity;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.Json.Data;
import com.example.myapp.R;
import com.example.myapp.adapter.NewsAdapter;
import com.example.myapp.Utils.DataUtils;
import com.example.myapp.Utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class SelectActivity extends BaseActivity implements View.OnClickListener {

   private ImageButton clear,select,goback;
   private EditText et;
   private TextView st;
    private List<Data> dataList;
    private DataUtils dataUtility;
    private RecyclerView recyclerView;
    private NewsAdapter adapter;
   String ss;
   int cont;


    @Override
    protected int initLayout() {
        return R.layout.activity_select;
    }

    @Override
    protected void initView() {

        dataUtility=new DataUtils(this);
        dataList=new ArrayList<>();
        recyclerView=findViewById(R.id.select_Rv);
        clear=findViewById(R.id.select_clear);
        select=findViewById(R.id.select_select);
        goback=findViewById(R.id.select_goback);
        et=findViewById(R.id.select_et);
        st=findViewById(R.id.select_cont);
        ss=getIntent().getStringExtra("select");
        clear.setOnClickListener(this);
        select.setOnClickListener(this);
        goback.setOnClickListener(this);
        search(ss);

    }

    @Override
    protected void initData() {
        et.setText("");
        et.setText(ss);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.select_select:
                if (StringUtils.isEmpty(ss)){
                    Toast.makeText(this, "请输入查询新闻的标题", Toast.LENGTH_SHORT).show();
                }else {
                    search(ss);
                }
                break;
            case R.id.select_goback:
                finish();
                break;
            case R.id.select_clear:
                et.setText("");
                break;
        }
    }

    public void search(String text){
        dataList=dataUtility.getTitle(text);
        cont=dataUtility.getTitleCount(text);
        if (cont>0){
            st.setText(""+cont);
        }else {
            st.setText("0");
        }
        adapter = new NewsAdapter(this, dataList,recyclerView);
        recyclerView.setAdapter(adapter);
        intentWeb(adapter,dataList);
    }

}