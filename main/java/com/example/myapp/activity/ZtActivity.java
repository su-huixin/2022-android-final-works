package com.example.myapp.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myapp.R;

public class ZtActivity extends BaseActivity {

    Button button,button2,button3;

    @Override
    protected int initLayout() {
        return R.layout.activity_zt;
    }

    @Override
    protected void initView() {
        button=findViewById(R.id.zt_button);
        button2=findViewById(R.id.zt_button2);
        button3=findViewById(R.id.zt_button3);

    }

    @Override
    protected void initData() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tck(3.0f);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tck(2.0f);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tck(1.0f);
            }
        });

    }

    private void tck(float stary){
        AlertDialog alertDialog = new AlertDialog.Builder(this) .setTitle("确定设置字体吗？")
                .setMessage("设置完成需要重启app才能生效，若有黑屏闪过为正常现象")
                .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    //添加"Yes"按钮
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        BaseActivity.staray=stary;
                        recreate();
                    }
                }).setNegativeButton("取消", new DialogInterface.OnClickListener() {//添加取消
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        alertDialog.show();
    }

}