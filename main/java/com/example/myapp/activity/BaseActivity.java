package com.example.myapp.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapp.Json.Data;
import com.example.myapp.R;
import com.example.myapp.adapter.NewsAdapter;

import java.util.List;

public abstract class BaseActivity extends AppCompatActivity {

    public static float staray=1.0f;
    public Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context=this;
        setContentView(initLayout());
        initView();
        initData();


    }



    @Override
    public Resources getResources() {
        Resources res = super.getResources();
        Configuration config = res.getConfiguration();
        config.fontScale = staray; //1 设置正常字体大小的倍数
        res.updateConfiguration(config, res.getDisplayMetrics());
        return res;
    }

    protected abstract int initLayout();//初始化布局

    protected abstract void initView();//初始化控件

    protected abstract void initData();//初始化数据

    public void showToast(String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    public void intentWeb(NewsAdapter adapter, List<Data> dataList) {
        adapter.setOnItemClickListener(new NewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, Data data) {
                String url = dataList.get(position).getUrl();
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtra("url", url);
                intent.putExtra("Uniquekey", data.getUniquekey());
                intent.putExtra("name", data.getUniquekey());
                startActivity(intent);
            }
        });
    }


    public void xsmm(boolean ischeck, EditText editText){
        //更改输入显示方式的方法
        if (ischeck){
            editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }else {
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

}
