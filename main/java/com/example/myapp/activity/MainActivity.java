package com.example.myapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapp.R;
import com.example.myapp.activity.BaseActivity;
import com.example.myapp.activity.LoginActivity;
import com.example.myapp.activity.RegisterActivity;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    Button login,register;

    @Override
    protected int initLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        login=findViewById(R.id.login_btn);
        register=findViewById(R.id.register_btn);
    }

    @Override
    protected void initData() {
        login.setOnClickListener(this);
        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent();
        switch (v.getId()){
            case R.id.login_btn:
               intent=new Intent(this,LoginActivity.class);
                break;
            case R.id.register_btn:
               intent=new Intent(this,RegisterActivity.class);
                break;
        }
        startActivity(intent);
    }
}