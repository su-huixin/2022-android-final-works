package com.example.myapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapp.R;
import com.example.myapp.Utils.Declare;
import com.example.myapp.Utils.StringUtils;
import com.example.myapp.Utils.UserUtils;
import com.example.myapp.entity.User;

public class ForgetActivity extends BaseActivity {

    private CheckBox forgetck;
    private Button forget;
    private EditText Name,NewPassage,Phone;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int initLayout() {
        return R.layout.activity_forget;
    }

    @Override
    protected void initView() {
        forgetck=findViewById(R.id.forget_ck);
        forget=findViewById(R.id.forget_forget);
        Name=findViewById(R.id.forget_username);
        NewPassage=findViewById(R.id.forget_password);
        Phone=findViewById(R.id.forget_phone);
        name=Declare.getInstance().getAccounts();
    }

    @Override
    protected void initData() {
        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newpass = NewPassage.getText().toString();
                String ph=Phone.getText().toString();
                String na=Name.getText().toString();

                if (StringUtils.isEmpty(na)||StringUtils.isEmpty(ph)||StringUtils.isEmpty(newpass)){
                    Toast.makeText(ForgetActivity.this, "请输入完全内容", Toast.LENGTH_SHORT).show();
                }else {
                    UserUtils userService=new UserUtils(ForgetActivity.this);
                    User user=new User(na,newpass,ph);
                    System.out.println(newpass+"1"+ph+"2"+"33333333333333"+na);
                    userService.UPDATE(user);
                    Toast.makeText(ForgetActivity.this, "密码修改成功", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(ForgetActivity.this, LoginActivity.class);
                    startActivity(intent);
                }


            }
        });

    }

    public void  Plaintext(View view){
        //更改密码显示方式的方法
        boolean a=false;
        a=forgetck.isChecked();
        xsmm(a,NewPassage);
    }

}