package com.example.myapp.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.example.myapp.R;
import com.example.myapp.Utils.Declare;
import com.example.myapp.entity.User_Collection;
import com.example.myapp.Utils.User_CollectionUtils;

public class WebActivity extends BaseActivity {

    private WebView webView;
    private Toolbar toolbar,ltoolBar;
    String url,Uniquekey;
    private User_CollectionUtils ucu;
    private ClipboardManager cm;
    private ClipData mClipData;



    @Override
    protected int initLayout() {
        return R.layout.activity_web;
    }

    @Override
    protected void initView() {
        webView = findViewById(R.id.webView);
        toolbar = findViewById(R.id.toolbar_webview);
        ltoolBar =  findViewById(R.id.toolbar_webcomment);
        ucu=new User_CollectionUtils(this);
        findViewById(R.id.toolbar_webcomment).bringToFront();
        //获取剪贴板管理器
        cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);



    }

    @Override
    protected void initData() {
        url = getIntent().getStringExtra("url");
        Uniquekey=getIntent().getStringExtra("Uniquekey");

        String name= Declare.getInstance().getAccounts();
        System.out.println("2222"+url);
        WebSettings settings = webView.getSettings();
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
//handler.cancel(); 默认的处理方式，WebView变成空白页
                handler.proceed();
//handleMessage(Message msg); 其他处理
            }

        });
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true); //支持缩放，默认为true。是下面那个的前提。
        settings.setBuiltInZoomControls(true); //设置内置的缩放控件。若为false，则该WebView不可缩放
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setLoadWithOverviewMode(true);
        /*settings.setDisplayZoomControls(false);*/
        webView.loadUrl(url);

        setSupportActionBar(ltoolBar);
        toolbar.setTitle("新闻");
        setSupportActionBar(toolbar);
        ltoolBar.inflateMenu(R.menu.tool_webbootom);
        ltoolBar.setTitle("感谢观看");
        ltoolBar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.news_share:
                        // 创建普通字符型ClipData
                        mClipData = ClipData.newPlainText("url", url);
                        // 将ClipData内容放到系统剪贴板里。
                        cm.setPrimaryClip(mClipData);
                        showToast("复制成功,可以分享给好友啦");
                        break;
                    case R.id.news_collect:
                        //下一步实现点击收藏功能，以及用户查看收藏功能
                        Toast.makeText(WebActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
                        Declare.getInstance().setStruk(Uniquekey);
                        User_Collection uc=new User_Collection(name,Uniquekey);
                        ucu.addcoll(uc);
                        break;

                }
                return true;
            }


        });
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.mipmap.collect);
        }
    }





    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent returnIntent = new Intent();
                WebActivity.this.finish();
                break;

            default:
                break;
        }
        return true;
    }


}