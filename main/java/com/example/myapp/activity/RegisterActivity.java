package com.example.myapp.activity;

import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapp.R;
import com.example.myapp.entity.User;
import com.example.myapp.Utils.UserUtils;

public class RegisterActivity extends BaseActivity implements View.OnClickListener{

    private Button register;
    private EditText name,pwd,qrpwd,phone;
    private String un,upwd;
    CheckBox pwdck,qrpwdck,ycck;


    @Override
    protected int initLayout() {
        return R.layout.activity_register;
    }

    @Override
    protected void initView() {
        register=findViewById(R.id.register_btn_register);
        name=findViewById(R.id.register_et_name);
        pwd=findViewById(R.id.register_et_pwd);
        qrpwd=findViewById(R.id.register_et_qrpwd);
        phone=findViewById(R.id.register_et_phone);
        pwdck=findViewById(R.id.register_ck);
        qrpwdck=findViewById(R.id.register_qrck);
        ycck=findViewById(R.id.ysyd);
    }

    @Override
    protected void initData() {
        register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        un=name.getText().toString();
        upwd=pwd.getText().toString();
        String qeupwd=qrpwd.getText().toString();
        String ph=phone.getText().toString();


        switch (v.getId()){
            case R.id.register_btn_register:
                register(un,upwd,qeupwd,ph);
                break;
        }
    }

    private void register(String account, String pwd,String qrmm,String ph) {

        if (account.equals("")||pwd.equals("")){
            showToast("请完整输入各项内容");
        }else if (account.length()<3||account.length()>10) {
            showToast("账号小于三个字符或大于十个字符，请重新输入");
        }else if (pwd.length()<6||pwd.length()>10) {
            showToast("密码小于六位数或大于15位数字，请重新输入");
        }else if (!qrmm.equals(pwd)) {
            Toast.makeText(RegisterActivity.this, "两次输入密码不一致，请重新输入", Toast.LENGTH_SHORT).show();
        }else if (ph.length()>11||ph.length()<11) {
            Toast.makeText(RegisterActivity.this, "电话是11位数字，请重新输入", Toast.LENGTH_SHORT).show();
        }else if (ph.length()>11||ph.length()<11) {
            Toast.makeText(RegisterActivity.this, "电话是11位数字，请重新输入", Toast.LENGTH_SHORT).show();
        } else if (ycck.isChecked()==false) {
            Toast.makeText(RegisterActivity.this, "请阅读并勾选隐私保护协议", Toast.LENGTH_SHORT).show();
        }else {
            User user=new User(account,pwd,ph);
            UserUtils userService=new UserUtils(RegisterActivity.this);
            boolean yz=userService.register(user);
            if (yz==false){
                showToast("账号已存在");
            }else {
                showToast("注册成功");
                RegisterActivity.this.finish();
            }
        }

    }

    public void  Plaintext(View view){
        //更改密码显示方式的方法
        boolean a=false;
        switch (view.getId()){
            case R.id.register_ck:
                a=pwdck.isChecked();
                xsmm(a,pwd);
                break;
            case R.id.register_qrck:
                a=qrpwdck.isChecked();
                xsmm(a,qrpwd);
                break;
        }
    }

}