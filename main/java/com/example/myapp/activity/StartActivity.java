package com.example.myapp.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapp.Animation.CustomAni;
import com.example.myapp.R;

public class StartActivity extends BaseActivity {

    private TextView textView;
    private ImageView imageView;

    @Override
    protected int initLayout() {
        return R.layout.activity_start;
    }

    @Override
    protected void initView() {
        textView=findViewById(R.id.statr_text);
        imageView=findViewById(R.id.start_yl);

    }

    @Override
    protected void initData() {

      dhjh(textView);


    }

    // 启动动画
    private void beginAnimation(View v) {
        CustomAni customAni = new CustomAni();
        customAni.setmRotateY(15);
        v.startAnimation(customAni);
        customAni.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Intent intent=new Intent(StartActivity.this,MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void dhjh(View v){
        AnimationSet aniSet = new AnimationSet(false);

        //缩放
        ScaleAnimation scale = new ScaleAnimation(0, 2, 2, 0);
        scale.setDuration(3000);
        aniSet.addAnimation(scale);

        RotateAnimation rotate = new RotateAnimation(0, 360,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(3000);
        aniSet.addAnimation(rotate);

        // 透明度动画
        AlphaAnimation alpha = new AlphaAnimation(0, 1);
        alpha.setDuration(4000);
        aniSet.addAnimation(alpha);

      /*  //移动动画
        TranslateAnimation translate = new TranslateAnimation(0, 160, 0, 240);
        translate.setDuration(4000);
        aniSet.addAnimation(translate);*/


        aniSet.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //动画开始

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //动画结束
                beginAnimation(imageView);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                //播放次数

            }
        });
        v.startAnimation(aniSet);


    }

    public void textdh(){
        AnimationSet aniSet = new AnimationSet(false);

}


}