package com.example.myapp.Utils;

import android.app.Application;

public class Declare extends Application  {

    private static Declare singleInstance;

    public static Declare  getInstance() {
       return singleInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleInstance=this;
    }

    private static String strAccounts;
    private static String strAccountsPWD;
    private static String struk;

    // 操作帐号
    public void setAccounts(String accountsNO) {
        this.strAccounts = accountsNO;
    }

    public String getAccounts() {
        return this.strAccounts;
    }

    // 操作密码
    public void setAccountsPassWord(String passWord) {
        this.strAccountsPWD = passWord;
    }

    public String getAccountsPassWord() {
        return this.strAccountsPWD;
    }


    public  String getStruk() {
        return struk;
    }

    public  void setStruk(String struk) {
        Declare.struk = struk;
    }
}
