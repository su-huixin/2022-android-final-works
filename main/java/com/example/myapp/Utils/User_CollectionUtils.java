package com.example.myapp.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapp.entity.User_Collection;

import java.util.ArrayList;
import java.util.List;

public class User_CollectionUtils {

    private DatabaseHelper dbHelper;

    public User_CollectionUtils(Context context) {
        dbHelper = new DatabaseHelper(context);
    }


    public List<String> getColl(String cname){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        List<String> l=new ArrayList();
        User_Collection uc = null;
        String sql="select * from user_collection where cname=?";
        Cursor cursor = db.rawQuery(sql, new String[]{cname});
        while (cursor.moveToNext()){
            String cuniquekey = cursor.getString(cursor.getColumnIndex("cuniquekey"));
            String name=cursor.getString(cursor.getColumnIndex("cname"));
            uc=new User_Collection(name,cuniquekey);
            l.add(cuniquekey);
        }
        return l;
    }

    public void addcoll(User_Collection uc){
        SQLiteDatabase db =dbHelper.getWritableDatabase();
        String sql = "select * from user_collection where cuniquekey=?";
        Cursor cursor = db.rawQuery(sql, new String[]{uc.getCuniquekey()});
        if (cursor.moveToFirst() == true) {
            cursor.close();
        }else {
            sql = " replace into user_collection(cname,cuniquekey) VALUES (?,?)";
            db.execSQL(sql,new Object[]{uc.getCnaem(),uc.getCuniquekey()});
        }

    }

    public int getCount(String name) {//计数方法
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        int count=0;
        Cursor cursor = db.rawQuery("SELECT  COUNT(cname) from user_collection where cname='"+name+"'",null);
        cursor.moveToNext();
           count = cursor.getInt(0);
        cursor.close();
        if (count!=0){
            return count;
        }else {
            return count=0;
        }
    }

    public boolean getSelect(String cuk){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "select * from user_collection where cuniquekey=?";
        Cursor cursor = db.rawQuery(sql, new String[]{cuk});
        if (cursor.moveToFirst() == true) {
            cursor.close();
            return true;
        }else {
            return false;
        }
    }
    public boolean Delect(String cuk){
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "delete from user_collection where cuniquekey=?";
       db.rawQuery(sql, new String[]{cuk});
       return true;
    }

}
