package com.example.myapp.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapp.entity.User;

public class UserUtils {

    //操作user表的工具类

    private DatabaseHelper dbHelper;
    private Context context;

    public UserUtils(Context context) {
        this.context=context;
        this.dbHelper = new DatabaseHelper(context);
    }

    public boolean login(String username, String userpwd) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "select * from user where username=? and userpwd=?";
        Cursor cursor = db.rawQuery(sql, new String[]{username, userpwd});
        if (cursor.moveToFirst() == true) {
            cursor.close();
            return true;
        }
        return false;
    }


    public boolean register(User user) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql;
        sql = "select * from user where username=?";
        Cursor cursor = db.rawQuery(sql, new String[]{user.getUsername()});
        if (cursor.moveToFirst() == true) {
            cursor.close();
            return false;
        } else {
           /* sql = "INSERT INTO user(username,userpwd,phone) VALUES ( '" + user.getUsername() + "', '" + user.getUserpwd() + "','" + user.getPhone() + "')";*/
            sql = "INSERT INTO user(username,userpwd,photo) VALUES ( '" + user.getUsername() + "', '" + user.getUserpwd() + "','"+user.getPhone()+"')";
            db.execSQL(sql);
            return true;
        }

    }

    public boolean UPDATE(User user) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "UPDATE user SET userpwd =? where photo = ? and username=?";
            db.execSQL(sql,new String[]{user.getUserpwd(),user.getPhone(),user.getUsername()});
        return true;
    }

    public void TextUD(String txt,String name) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "UPDATE user SET txt = '"+txt+"'WHERE username= '"+name+"'";
        db.execSQL(sql);

    }
    public String TextSelect(String name) {
        String utxt=null;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql = "select txt from user where username=? ";
        Cursor cursor = db.rawQuery(sql, new String[]{name});
        if (cursor.moveToFirst() == true) {
            utxt=cursor.getString(cursor.getColumnIndex("txt"));
            cursor.close();
            return utxt;
        }
        return null;
    }


}
