package com.example.myapp.Utils;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myapp.Json.Data;

import java.util.ArrayList;
import java.util.List;

public class DataUtils {

    private DatabaseHelper dbHelper;

    public DataUtils(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public List<Data> getData(String key,String valus) {//单个条件精确查询多条数据
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        List<Data> list=new ArrayList<>();
        String sql="select * from news where "+key+"=? order by datetime(date) desc";
        Cursor cursor = db.rawQuery(sql, new String[]{valus});
       while (cursor.moveToNext()){
            String uniquekey = cursor.getString(cursor.getColumnIndex("uniquekey"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            String category = cursor.getString(cursor.getColumnIndex("category"));
            String author_name = cursor.getString(cursor.getColumnIndex("author_name"));
            String url = cursor.getString(cursor.getColumnIndex("url"));
            String thumbnail_pic_s = cursor.getString(cursor.getColumnIndex("thumbnail_pic_s"));
            Data data=new Data(uniquekey,title,date,category,author_name,url,thumbnail_pic_s);
            list.add(data);
        }
        return list;
    }

    public Data getDataone(String uk) {//单个条件精确查询一条数据
        Data data=new Data();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql="select * from news where uniquekey=? order by datetime(date) desc";
        Cursor cursor = db.rawQuery(sql, new String[]{uk});
        while (cursor.moveToNext()){
            String uniquekey = cursor.getString(cursor.getColumnIndex("uniquekey"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            String category = cursor.getString(cursor.getColumnIndex("category"));
            String author_name = cursor.getString(cursor.getColumnIndex("author_name"));
            String url = cursor.getString(cursor.getColumnIndex("url"));
            String thumbnail_pic_s = cursor.getString(cursor.getColumnIndex("thumbnail_pic_s"));
             data=new Data(uniquekey,title,date,category,author_name,url,thumbnail_pic_s);
        }
        return data;
    }

    public void add(Data data){//添加
        SQLiteDatabase db =dbHelper.getWritableDatabase();
        String sql;
        sql = "select * from news where uniquekey=?";
        Cursor cursor = db.rawQuery(sql, new String[]{data.getUniquekey()});
        if (cursor.moveToFirst() == true) {
            cursor.close();
        }else {
            sql = " replace into news(uniquekey,title,date,category,author_name,url,thumbnail_pic_s) VALUES (?,?,?,?,?,?,?)";
            db.execSQL(sql,new Object[]{data.getUniquekey(),data.getTitle(),data.getDate(),data.getCategory(),data.getAuthor_name(),data.getUrl(),data.getThumbnail_pic_s()});
        }


    }

    public List<Data> getTitle(String mtitle) {//标题的模糊查询
        Data data=new Data();
        List<Data> list=new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql="select * from news where title LIKE'%"+mtitle+"%' order by datetime(date) desc";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()){
            String uniquekey = cursor.getString(cursor.getColumnIndex("uniquekey"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            String category = cursor.getString(cursor.getColumnIndex("category"));
            String author_name = cursor.getString(cursor.getColumnIndex("author_name"));
            String url = cursor.getString(cursor.getColumnIndex("url"));
            String thumbnail_pic_s = cursor.getString(cursor.getColumnIndex("thumbnail_pic_s"));
            data=new Data(uniquekey,title,date,category,author_name,url,thumbnail_pic_s);
            list.add(data);
        }
        return list;
    }

    public int getTitleCount(String mtitle) {//标题的模糊查询返回的结果个数
        Data data=new Data();
        List<Data> list=new ArrayList<>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String sql="select * from news where title LIKE'%"+mtitle+"%' order by datetime(date) desc";
        Cursor cursor = db.rawQuery(sql, null);
        while (cursor.moveToNext()){
            String uniquekey = cursor.getString(cursor.getColumnIndex("uniquekey"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String date = cursor.getString(cursor.getColumnIndex("date"));
            String category = cursor.getString(cursor.getColumnIndex("category"));
            String author_name = cursor.getString(cursor.getColumnIndex("author_name"));
            String url = cursor.getString(cursor.getColumnIndex("url"));
            String thumbnail_pic_s = cursor.getString(cursor.getColumnIndex("thumbnail_pic_s"));
            data=new Data(uniquekey,title,date,category,author_name,url,thumbnail_pic_s);
            list.add(data);
        }
        return list.size();
    }
}
