package com.example.myapp.Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    static String name="demo.db";
    static int dbVersion=1;


    public DatabaseHelper(Context context) {
        super(context, name, null, dbVersion);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //用户表
        db.execSQL("create table user(username text primary key ,"+
                "photo text not null," +
                "userpwd text not null," +
                "txt text)");
        //新闻表
        db.execSQL("create table news(uniquekey text primary key,"+
                "title text not null," +
                "date datetime not null," +
                "category text not null," +
                "author_name text not null," +
                "url text not null," +
                "thumbnail_pic_s blob not null)");
        //收藏表
        db.execSQL("create table user_collection(cuniquekey text primary key,"+
                "cname text not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
