package com.example.myapp.Animation;

import android.graphics.Camera;
import android.graphics.Matrix;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.Transformation;

public class CustomAni extends Animation {
    //3D旋转动画
    private int mCenWidth,mCenHeight;
    private Camera mCamera=new Camera();
    private float mRotateY=0.0f;

    @Override
    public void initialize(int width, int height, int parentWidth, int parentHeight) {
        super.initialize(width, height, parentWidth, parentHeight);
        //设置默认时长
        setDuration(9000);
        //动画保持至结束状态
        setFillAfter(true);
        //默认插槽器
        setInterpolator(new BounceInterpolator());//回弹效果
        mCenWidth=width/2;
        mCenHeight=height/2;
    }

    public void setmRotateY(float rotateY){
        this.mRotateY=rotateY;
    }

    // 自定义动画的核心，在动画的执行过程中会不断回调此方法，并且每次回调interpolatedTime值都在不断变化(0----1)f
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
        final Matrix matrix=t.getMatrix();
        mCamera.save();
        // 使用Camera设置Y轴方向的旋转角度
        mCamera.rotateY(mRotateY * interpolatedTime);
        // 将旋转变化作用到matrix上
        mCamera.getMatrix(matrix);
        mCamera.restore();
        // 通过pre方法设置矩阵作用前的偏移量来改变旋转中心
        matrix.preTranslate(mCenWidth, mCenHeight);// 在旋转之前开始位移动画
        matrix.postTranslate(-mCenWidth, -mCenHeight);// 在旋转之后开始位移动画
    }
}
