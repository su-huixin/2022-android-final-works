package com.example.myapp.view;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

public class FixeViewPager extends ViewPager {

    public FixeViewPager(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setCurrentItem(int item) {
        //关闭动画效果
        super.setCurrentItem(item,false);
    }

    public FixeViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
