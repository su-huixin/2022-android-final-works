package com.example.myapp.fragment;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;


import com.example.myapp.R;
import com.example.myapp.activity.SelectActivity;
import com.example.myapp.adapter.HomeAdapter;
import com.example.myapp.Utils.StringUtils;
import com.flyco.tablayout.SlidingTabLayout;


import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends BaseFragment {

    private List<Fragment> mFragments ;
    private final String[] mTitles = {
            "头条新闻", "社会新闻", "国际新闻"
            , "娱乐新闻", "军事新闻", "科技新闻", "财经新闻"
    };
    private ViewPager mViewPager;
    private SlidingTabLayout slidingTabLayout;
    private HomeAdapter homeAdapter;
    private EditText select;
    private ImageButton imagbtn;


    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }




    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected int initLayout() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initView() {
        mViewPager=mRootView.findViewById(R.id.fragment_home_FixeViewPager);
        slidingTabLayout=mRootView.findViewById(R.id.fragment_home_STL);
        select=mRootView.findViewById(R.id.fragment_home_et);
        imagbtn=mRootView.findViewById(R.id.fragment_home_imagbtn);
        mFragments=new ArrayList<>();

    }

    @Override
    protected void initData() {


        imagbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String se=select.getText().toString();
                System.out.println(se);
                if (StringUtils.isEmpty(se)){
                    Toast.makeText(getActivity(), "请输入查询新闻的标题", Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(getActivity(), "s", Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(getActivity(), SelectActivity.class);
                    intent.putExtra("select",se);
                    startActivity(intent);
                }

            }
        });



        mFragments.add(new NewsFragment());
        mFragments.add(new SocietyFragment());
        mFragments.add(new InternationalFragment());
        mFragments.add(new AmusementFragment());
        mFragments.add(new MilitaryFragment());
        mFragments.add(new KeJiFragment());
        mFragments.add(new CaiJingFragment());

        mViewPager.setOffscreenPageLimit(mFragments.size());
        homeAdapter = new HomeAdapter(getFragmentManager(),mTitles, (ArrayList<Fragment>) mFragments);
        mViewPager.setAdapter(homeAdapter);

        slidingTabLayout.setViewPager(mViewPager);
    }

}