package com.example.myapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapp.Json.Data;
import com.example.myapp.Json.News;
import com.example.myapp.activity.WebActivity;
import com.example.myapp.adapter.NewsAdapter;
import com.example.myapp.Utils.DataUtils;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public abstract class BaseFragment extends Fragment {

    protected View mRootView;
    public static String API_URL = "http://v.juhe.cn/toutiao/index?type=";
    public static String API_KEY = "&key=307a185e91fcd5121906bce158d39f67";
    private int states = 3;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            mRootView = inflater.inflate(initLayout(), container, false);
            initView();
            initData();
        return mRootView;
    }

    protected abstract int initLayout();//初始化布局

    protected abstract void initView();//初始化控件

    protected abstract void initData();//初始化数据

    public void navigateTo(Class c){
        //跳转方法
        Intent intent=new Intent(getActivity(),c);
        startActivity(intent);

    }

    public void sendRequestWithOKHttp(DataUtils dataUtility, NewsAdapter adapter, String category){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            //http://v.juhe.cn/toutiao/index?type=top&key=58ff2000704bbf8e31ce031c1b555a33
                            //http://v.juhe.cn/toutiao/index?type=top&key=468a538795ca302846f994e7559df8a7
                            //API_URL+category+API_KEY
                            //http://v.juhe.cn/toutiao/index?type="+data+"&key=547ee75ef186fc55a8f015e38dcfdb9a
                            .url("http://v.juhe.cn/toutiao/index?type="+category+"&key=58ff2000704bbf8e31ce031c1b555a33")
                            .build();
                    Response response = null;
                    response = client.newCall(request).execute();
                    String responseData = response.body().string();
                    Log.d("测试：", responseData);
                    parseJsonWithGson(responseData,dataUtility,adapter);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void parseJsonWithGson(String jsonData, DataUtils dataUtility, NewsAdapter adapter){
        Gson gson = new Gson();

        //gson.fromJson(jsonData, News.class);
        News news = gson.fromJson(jsonData, News.class);
        List<Data> list = news.getResult().getData();
        for (int i=0; i<list.size(); i++){
            String uniquekey = list.get(i).getUniquekey();
            String title = list.get(i).getTitle();
            String date = list.get(i).getDate();
            String category = list.get(i).getCategory();
            String author_name = list.get(i).getAuthor_name();
            String url = list.get(i).getUrl();
            String thumbnail_pic_s= list.get(i).getThumbnail_pic_s();
            Data data=new Data(uniquekey,title,date,category,author_name,url,thumbnail_pic_s);
            dataUtility.add(data);
        }

       /* //更新Adapter(务必在主线程中更新UI!!!)
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });*/
    }

    public void intentWeb(NewsAdapter adapter, List<Data> dataList){
        adapter.setOnItemClickListener(new NewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, Data data) {
                String url = dataList.get(position).getUrl();
                Intent intent=new Intent(getActivity(), WebActivity.class);
                intent.putExtra("url",url);
                intent.putExtra("Uniquekey",data.getUniquekey());
                intent.putExtra("name",data.getUniquekey());
                startActivity(intent);
            }
        });
    }

    public boolean isConnectIsNomarl() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info != null && info.isAvailable()) {
            String intentName = info.getTypeName();
            Log.i("通了没！", "当前网络名称：" + intentName);
            return true;
        } else {
            Log.i("通了没！", "没有可用网络");
            return false;
        }
    }

    }

