package com.example.myapp.fragment ;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.Json.Data;
import com.example.myapp.R;
import com.example.myapp.Utils.DatabaseHelper;
import com.example.myapp.Utils.Declare;
import com.example.myapp.activity.BaseActivity;
import com.example.myapp.activity.WebActivity;
import com.example.myapp.adapter.NewsAdapter;
import com.example.myapp.Utils.DataUtils;
import com.example.myapp.Utils.User_CollectionUtils;

import java.util.ArrayList;
import java.util.List;


public class CollectFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private TextView colltv;
    private User_CollectionUtils ucu;
    private NewsAdapter adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<Data> dataList;
    private DataUtils dataUtility;
    List cuk=new ArrayList();
    String name;


    public CollectFragment() {
        // Required empty public constructor
    }

    public static Fragment newInstance() {
        CollectFragment collectFragment=new CollectFragment();
        return collectFragment;
    }


    @Override
    protected int initLayout() {
        return R.layout.fragment_collect;
    }

    @Override
    protected void initView() {

        swipeRefreshLayout=mRootView.findViewById(R.id.coll_swipeRefreshLayout);
        recyclerView=mRootView.findViewById(R.id.fragment_coll_Rv);
        dataUtility=new DataUtils(getActivity());
        dataList=new ArrayList<>();
        ucu=new User_CollectionUtils(getActivity());
        colltv=mRootView.findViewById(R.id.coll_cont);
        name= Declare.getInstance().getAccounts();
        cuk=ucu.getColl(name);
        senddh();

    }

    @Override
    protected void initData() {
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
        //设置垂直排列
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }



    private void senddh(){

        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        //设置下拉时圆圈的背景颜色（这里设置成白色）
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(android.R.color.white);

        //设置下拉刷新时的操作
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SwipeIsFinish();
            }
        });

        //启动自动刷新
        swipeRefreshLayout.measure(0,0);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.post(new Runnable() {
              @Override
            public void run() {
                if (swipeRefreshLayout.isRefreshing()){
                    int cont=ucu.getCount(name);
                    System.out.println(cont+"/////////////////////////");
                    if (cont==0){
                        colltv.setText("0");
                    }else {
                        colltv.setText(" "+cont);

                }
                    for (int i=0;i<cuk.size();i++){
                        String cukey=cuk.get(i).toString();
                        Data a=dataUtility.getDataone(cukey);
                        dataList.add(a);
                    }
                    adapter = new NewsAdapter(getActivity(), dataList,recyclerView);
                    recyclerView.setAdapter(adapter);
                    intentWeb();
                    swipeRefreshLayout.setRefreshing(false);
            }
        }
    });
    }
    public void SwipeIsFinish(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        int cont=ucu.getCount(name);
                        if (cont<=0){
                            colltv.setText("0");
                        }else {
                            cuk=ucu.getColl(name);
                            colltv.setText(""+cont);
                            dataList.clear();
                            for (int i=0;i<cuk.size();i++){
                                String cukey=cuk.get(i).toString();
                                Data a=dataUtility.getDataone(cukey);
                                dataList.add(a);
                            }
                            LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
                            //设置垂直排列
                            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                            recyclerView.setLayoutManager(linearLayoutManager);
                           adapter.notifyDataSetChanged();
                            intentWeb();
                            swipeRefreshLayout.setRefreshing(false);
                        }

                    }
                });
            }
        }).start();
    }
    private void intentWeb(){
        adapter.setOnItemClickListener(new NewsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, Data data) {
                Toast.makeText(getActivity(), data.toString(), Toast.LENGTH_SHORT).show();
                String url = dataList.get(position).getUrl();
                Intent intent=new Intent(getActivity(), WebActivity.class);
                intent.putExtra("url",url);
                intent.putExtra("Uniquekey",data.getUniquekey());
                intent.putExtra("name",data.getUniquekey());
                startActivity(intent);
            }
        });
    }

}