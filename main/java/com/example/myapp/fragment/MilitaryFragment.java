package com.example.myapp.fragment;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapp.Json.Data;
import com.example.myapp.R;
import com.example.myapp.adapter.NewsAdapter;
import com.example.myapp.Utils.DataUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class MilitaryFragment extends BaseFragment {

    private RecyclerView recyclerView;
    private static final int UPNEWS_INSERT = 0;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton topbtn;
    private NewsAdapter adapter;
    private List<Data> dataList;
    private DataUtils dataUtility;

    private int count;

    public MilitaryFragment() {
    }



    @Override
    protected int initLayout() {
        return R.layout.fragment_military;
    }

    @Override
    protected void initView() {
        dataUtility=new DataUtils(getActivity());
        recyclerView=mRootView.findViewById(R.id.fragment_mil_Rv);
        swipeRefreshLayout=mRootView.findViewById(R.id.mil_swipeRefreshLayout);
        topbtn=mRootView.findViewById(R.id.fragment_mil_floatingbtn);
        senddh();
    }

    @Override
    protected void initData() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        topbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);

            }
        });
    }


    //下拉刷新和启动刷新
    private void senddh() {
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        //设置下拉时圆圈的背景颜色（这里设置成白色）
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(android.R.color.white);

        //设置下拉刷新时的操作
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SwipeIsFinish();
            }
        });
        swipeRefreshLayout.measure(0,0);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout.isRefreshing()){
                    sendRequestWithOKHttp(dataUtility,adapter,"junshi");
                    dataList=dataUtility.getData("category","军事");
                    adapter = new NewsAdapter(getActivity(), dataList,recyclerView);
                    recyclerView.setAdapter(adapter);
                    intentWeb(adapter,dataList);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    public void SwipeIsFinish(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        sendRequestWithOKHttp(dataUtility,adapter,"junshi");
                        dataList=dataUtility.getData("category","军事");
                        adapter = new NewsAdapter(getActivity(), dataList,recyclerView);
                        recyclerView.setAdapter(adapter);
                        intentWeb(adapter,dataList);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        }).start();
    }



    }