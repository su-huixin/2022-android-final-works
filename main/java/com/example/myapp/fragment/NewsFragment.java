package com.example.myapp.fragment;


import android.view.View;
import android.widget.Toast;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapp.Json.Data;
import com.example.myapp.R;
import com.example.myapp.adapter.NewsAdapter;

import com.example.myapp.Utils.DataUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.List;


public class NewsFragment extends BaseFragment{


    private RecyclerView recyclerView;
    private static final int UPNEWS_INSERT = 0;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton topbtn;
    private NewsAdapter adapter;
    private List<Data> dataList;
    private DataUtils dataUtility;
    private static String t2;


    private int count;


    public NewsFragment() {
    }



    @Override
    protected int initLayout() {
        return R.layout.fragment_news;
    }

    @Override
    protected void initView() {

        dataUtility=new DataUtils(getActivity());
        recyclerView=mRootView.findViewById(R.id.fragment_top_Rv);
        swipeRefreshLayout=mRootView.findViewById(R.id.swipeRefreshLayout);
        topbtn=mRootView.findViewById(R.id.fragment_btn_floatingbtn);
        senddh();


    }

    @Override
    protected void initData() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        topbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);

            }
        });
    }


    private void senddh(){
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_light,
                android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        //设置下拉时圆圈的背景颜色（这里设置成白色）
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(android.R.color.white);

        //设置下拉刷新时的操作
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                SwipeIsFinish();
            }
        });
        swipeRefreshLayout.measure(0,0);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (swipeRefreshLayout.isRefreshing()){
                    boolean wl=isConnectIsNomarl();
                    if (wl){
                        sendRequestWithOKHttp(dataUtility,adapter,"top");
                        dataList=dataUtility.getData("category","头条");
                        adapter = new NewsAdapter(getActivity(), dataList,recyclerView);
                        recyclerView.setAdapter(adapter);
                        intentWeb(adapter,dataList);
                        swipeRefreshLayout.setRefreshing(false);
                    }else {
                        Toast.makeText(getActivity(), "刷新失败，网络未连接", Toast.LENGTH_SHORT).show();
                    }

                }
                /*    sendRequestWithOKHttp(dataUtility,adapter,"top");
                    dataList=dataUtility.getData("category","头条");
                    adapter = new NewsAdapter(getActivity(), dataList,recyclerView);
                    recyclerView.setAdapter(adapter);
                    intentWeb(adapter,dataList);
                    swipeRefreshLayout.setRefreshing(false);*/
            }
        });
    }


    public void SwipeIsFinish(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean wl=isConnectIsNomarl();
                        if (wl){
                            adapter.notifyDataSetChanged();
                            sendRequestWithOKHttp(dataUtility,adapter,"top");
                            dataList=dataUtility.getData("category","头条");
                            adapter = new NewsAdapter(getActivity(), dataList,recyclerView);
                            recyclerView.setAdapter(adapter);
                            intentWeb(adapter,dataList);
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            Toast.makeText(getActivity(), "刷新失败，网络未连接", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        }).start();
    }





}
