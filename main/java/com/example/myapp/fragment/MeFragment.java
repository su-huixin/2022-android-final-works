package com.example.myapp.fragment;

import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapp.R;
import com.example.myapp.Utils.Declare;
import com.example.myapp.Utils.StringUtils;
import com.example.myapp.Utils.UserUtils;
import com.example.myapp.activity.ForgetActivity;
import com.example.myapp.activity.ZtActivity;



/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MeFragment extends BaseFragment implements View.OnClickListener{


    private TextView username,ztsz,zwjs,me_text,unpwd;
    private ImageView head;
    private UserUtils userUtils;
    private Button finlogin;
    String name,utxt;




    public static MeFragment newInstance() {
        MeFragment fragment = new MeFragment();
        return fragment;
    }


    @Override
    protected int initLayout() {
        return R.layout.fragment_me;
    }

    @Override
    protected void initView() {
        username = mRootView.findViewById(R.id.me_name);
        head = mRootView.findViewById(R.id.me_head);
        ztsz=mRootView.findViewById(R.id.me_ztsz);
        zwjs=mRootView.findViewById(R.id.me_zwjs);
        me_text=mRootView.findViewById(R.id.me_text);
        unpwd=mRootView.findViewById(R.id.me_unpwd);
        userUtils=new UserUtils(getActivity());
        finlogin=mRootView.findViewById(R.id.me_fin);
    }

    @Override
    protected void initData() {

        name = Declare.getInstance().getAccounts();
        username.setText(name);
        utxt=userUtils.TextSelect(name);

        if (StringUtils.isEmpty(utxt)){
            me_text.setText("自我介绍一下吧");
        }else {
            me_text.setText(utxt);
        }

        ztsz.setOnClickListener(this);
        zwjs.setOnClickListener(this);
        unpwd.setOnClickListener(this);
        finlogin.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.me_ztsz:
                Intent intent=new Intent(getActivity(), ZtActivity.class);
                startActivity(intent);
                break;
            case R.id.me_zwjs:
                System.out.println("修改自我介绍");
                AlertDialog.Builder builder=new AlertDialog.Builder(getActivity())
                        .setTitle("修改自我介绍");
                EditText et=new EditText(getActivity());
                builder.setView(et);
                builder.setPositiveButton("取消",null);
                builder.setNegativeButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String lstxt=et.getText().toString();
                        if (StringUtils.isEmpty(lstxt)){
                            Toast.makeText(getActivity(), "请输入文字", Toast.LENGTH_SHORT).show();
                        }else {
                            userUtils.TextUD(lstxt,name);
                            me_text.setText(lstxt);
                        }
                    }
                });
                builder.show();
                break;
            case R.id.me_unpwd:
                Intent intent1=new Intent(getActivity(), ForgetActivity.class);
                startActivity(intent1);
                break;
            case R.id.me_fin:
                System.exit(0);
                break;
        }
    }


}