package com.example.myapp.entity;

public class User_Collection {
    String cnaem;
    String cuniquekey;

    public User_Collection() {
    }

    public User_Collection(String cnaem, String cuniquekey) {
        this.cnaem = cnaem;
        this.cuniquekey = cuniquekey;
    }

    public String getCnaem() {
        return cnaem;
    }

    public void setCnaem(String cnaem) {
        this.cnaem = cnaem;
    }

    public String getCuniquekey() {
        return cuniquekey;
    }

    public void setCuniquekey(String cuniquekey) {
        this.cuniquekey = cuniquekey;
    }
}
