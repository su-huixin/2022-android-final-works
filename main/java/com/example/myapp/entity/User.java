package com.example.myapp.entity;

public class User {

    //接收数据的类

    String username;
    String userpwd;
    String photo;
    String txt;

    public User(String username, String userpwd, String photo, String txt) {
        this.username = username;
        this.userpwd = userpwd;
        this.photo = photo;
        this.txt = txt;
    }

    public User(String username, String userpwd, String photo) {
        this.username = username;
        this.userpwd = userpwd;
        this.photo = photo;
    }
public User(String username, String userpwd) {
    this.username = username;
    this.userpwd = userpwd;

}
    public User() {

    }

    @Override
    public String toString() {
        return "User{" +
                " username='" + username + '\'' +
                ", userpwd='" + userpwd + '\'' +
                ", phonr='" + photo + '\'' +
                '}';
    }



    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

    public String getPhone() {
        return photo;
    }

    public void setPhone(String phone) {
        this.photo = phone;
    }

    public String getTxt() {
        return txt;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }
}
